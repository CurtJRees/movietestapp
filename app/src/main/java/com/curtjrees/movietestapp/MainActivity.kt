package com.curtjrees.movietestapp

import android.os.Bundle
import android.view.LayoutInflater
import com.curtjrees.movietestapp.common.BaseActivity
import com.curtjrees.movietestapp.databinding.ActivityMainBinding
import com.curtjrees.movietestapp.movie_list.MovieListFragment
import com.curtjrees.movietestapp.utils.switchFragment

class MainActivity : BaseActivity<ActivityMainBinding>() {

    override fun onInflateBinding(inflater: LayoutInflater): ActivityMainBinding = ActivityMainBinding.inflate(inflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        if (savedInstanceState == null) {
            switchFragment(MovieListFragment.newInstance(), true)
        }
    }
}


