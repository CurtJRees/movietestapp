package com.curtjrees.movietestapp.common

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseActivity<T: ViewBinding>: AppCompatActivity() {

    internal lateinit var binding: T

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = onInflateBinding(layoutInflater)
        setContentView(binding.root)
    }

    abstract fun onInflateBinding(inflater: LayoutInflater): T

}