package com.curtjrees.movietestapp.common

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseFragment<T : ViewBinding> : Fragment() {

    private var _binding: T? = null
    internal val binding: T
        get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? = onInflateBinding(inflater).also { _binding = it }.root

    abstract fun onInflateBinding(inflater: LayoutInflater): T

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}

