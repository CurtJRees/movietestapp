package com.curtjrees.movietestapp.common

import kotlinx.coroutines.flow.flow

data class Resource<out T>(val status: Status, val data: T?, val error: Throwable?) {

    companion object {
        fun <T> loading(data: T?): Resource<T> {
            return Resource(Status.LOADING, data, null)
        }

        fun <T> success(data: T?): Resource<T> {
            return Resource(Status.SUCCESS, data, null)
        }

        fun <T> error(data: T?, error: Throwable): Resource<T> {
            return Resource(Status.ERROR, data, error)
        }
    }

    enum class Status {
        SUCCESS,
        ERROR,
        LOADING
    }
}

fun <T> simpleResourceFlow(previousData: T?, fetchData: suspend () -> T) = flow {
    emit(Resource.loading(data = previousData))
    try {
        emit(Resource.success(data = fetchData.invoke()))
    } catch (e: Exception) {
        emit(Resource.error(data = previousData, error = e))
    }
}