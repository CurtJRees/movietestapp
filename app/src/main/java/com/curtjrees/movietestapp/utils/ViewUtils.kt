package com.curtjrees.movietestapp.utils

import android.view.View

fun View.setVisible(shouldBeVisible: Boolean = true, otherwise: Int = View.GONE) {
    this.visibility = if (shouldBeVisible) View.VISIBLE else otherwise
}