package com.curtjrees.movietestapp.utils

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Job
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

fun <T> LiveData<T>.debounce(duration: Long = 300L, coroutineScope: CoroutineScope, predicate: (T) -> Boolean = { true }) = MediatorLiveData<T>().also { mld ->

    val source = this
    var job: Job? = null

    mld.addSource(source) {
        job?.cancel()
        job = coroutineScope.launch {
            if (predicate.invoke(it)) delay(duration)
            mld.value = source.value
        }
    }
}

fun <T> mutableLiveData(initialValue: T): MutableLiveData<T> = MutableLiveData<T>().apply {
    value = initialValue
}