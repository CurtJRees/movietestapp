package com.curtjrees.movietestapp.utils

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.curtjrees.movietestapp.R

fun AppCompatActivity.switchFragment(fragment: Fragment, rootView: Boolean = false) {
    supportFragmentManager.commit {
        if (rootView) clearBackStack()
        replace(R.id.fragmentContainerView, fragment)
        if (!rootView) addToBackStack(fragment.javaClass.name)
    }
}

fun AppCompatActivity.clearBackStack() {
    (1..supportFragmentManager.backStackEntryCount).forEach { _ ->
        try {
            supportFragmentManager.popBackStack()
        } catch (e: IllegalStateException) {
            // There's no way of avoiding this if saveInstanceState has already been called
            //                Timber.e(e) TODO: Add Timber
        }
    }
}

