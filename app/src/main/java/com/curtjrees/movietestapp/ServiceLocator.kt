package com.curtjrees.movietestapp

import com.curtjrees.movietestapp.movie_list.MovieRepository
import com.curtjrees.movietestapp.movie_list.data.MovieApiService
import com.curtjrees.movietestapp.movie_list.domain.MovieDataSource
import com.squareup.moshi.Moshi
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ServiceLocator {

    val moshi: Moshi by lazy {
        Moshi.Builder().build()
    }

    val moshiConverterFactory: MoshiConverterFactory by lazy {
        MoshiConverterFactory.create(moshi)
    }

    val movieApiService: MovieApiService by lazy {
        Retrofit.Builder()
            .baseUrl("https://api.themoviedb.org/3/")
            .client(OkHttpClient())
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .build()
            .create(MovieApiService::class.java)
    }

    val movieRepository: MovieDataSource by lazy {
        MovieRepository(movieApiService)
    }

}