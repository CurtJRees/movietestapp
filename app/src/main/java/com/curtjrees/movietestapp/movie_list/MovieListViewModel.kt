package com.curtjrees.movietestapp.movie_list

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewModelScope
import com.curtjrees.movietestapp.common.Resource
import com.curtjrees.movietestapp.common.simpleResourceFlow
import com.curtjrees.movietestapp.movie_list.domain.Movie
import com.curtjrees.movietestapp.movie_list.domain.MovieDataSource
import com.curtjrees.movietestapp.utils.debounce
import com.curtjrees.movietestapp.utils.mutableLiveData
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class MovieListViewModel(
    private val movieRepository: MovieDataSource
) : ViewModel() {

    private val _viewStateLiveData: MutableLiveData<ViewState> = mutableLiveData(ViewState())
    private val currentViewState: ViewState get() = _viewStateLiveData.value!!
    val viewStateLiveData: LiveData<ViewState> = _viewStateLiveData.debounce(coroutineScope = viewModelScope) {
        it.movies?.status == Resource.Status.LOADING
    }

    fun getPopularMovies() = viewModelScope.launch {
        //        simpleResourceFlow(null) { movieRepository.getPopularMovies() }
        simpleResourceFlow(null, movieRepository::getPopularMovies)
            .collect {
                _viewStateLiveData.value = currentViewState.copy(movies = it)
            }
    }


    data class ViewState(
        val movies: Resource<List<Movie>>? = null
    )

    class Factory(
        private val movieRepository: MovieDataSource
    ) : ViewModelProvider.Factory {
        override fun <T : ViewModel?> create(modelClass: Class<T>): T = MovieListViewModel(movieRepository) as T
    }
}

