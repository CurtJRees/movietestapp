package com.curtjrees.movietestapp.movie_list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.curtjrees.movietestapp.databinding.ItemMovieBinding
import com.curtjrees.movietestapp.movie_list.domain.Movie

class MovieListAdapter(
    private val onClickAction: ((Movie) -> Unit)? = null
) : ListAdapter<Movie, RecyclerView.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MovieViewHolder.create(parent, onClickAction)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val item = getItem(position)) {
            is Movie -> (holder as MovieViewHolder).bind(item)
            else -> throw Exception("Unknown View Type")
        }
    }

    class MovieViewHolder(
        private val binding: ItemMovieBinding,
        private val onClickAction: ((Movie) -> Unit)?
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(movie: Movie) {
            with(binding) {
                titleTextView.text = movie.title

                root.setOnClickListener {
                    onClickAction?.invoke(movie)
                }
            }
        }

        companion object {
            fun create(parent: ViewGroup, onClickAction: ((Movie) -> Unit)?) =
                MovieViewHolder(
                    ItemMovieBinding.inflate(LayoutInflater.from(parent.context), parent, false),
                    onClickAction
                )
        }
    }


    companion object {
        private val diffCallback = object : DiffUtil.ItemCallback<Movie>() {
            override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
                //TODO: Do this
                return false
            }

            override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean =
                oldItem.hashCode() == newItem.hashCode()
        }
    }

}