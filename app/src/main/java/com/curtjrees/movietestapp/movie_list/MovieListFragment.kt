package com.curtjrees.movietestapp.movie_list

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.curtjrees.movietestapp.R
import com.curtjrees.movietestapp.ServiceLocator
import com.curtjrees.movietestapp.common.BaseFragment
import com.curtjrees.movietestapp.common.Resource
import com.curtjrees.movietestapp.databinding.FragmentMovieListBinding
import com.curtjrees.movietestapp.utils.setVisible
import kotlinx.android.synthetic.main.fragment_movie_list.*

class MovieListFragment private constructor() : BaseFragment<FragmentMovieListBinding>() {

    private val viewModel: MovieListViewModel by viewModels { MovieListViewModel.Factory(ServiceLocator.movieRepository) }

    override fun onInflateBinding(inflater: LayoutInflater): FragmentMovieListBinding = FragmentMovieListBinding.inflate(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val listAdapter = MovieListAdapter {
            Toast.makeText(requireContext(), it.title, Toast.LENGTH_SHORT).show()
        }
        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = listAdapter
        }

        binding.swipeRefreshLayout.setOnRefreshListener {
            viewModel.getPopularMovies()
        }
        binding.swipeRefreshLayout.setColorSchemeColors(Color.WHITE)
        binding.swipeRefreshLayout.setProgressBackgroundColorSchemeColor(ContextCompat.getColor(requireContext(), R.color.colorPrimary))

        viewModel.viewStateLiveData.observe(viewLifecycleOwner, Observer {
            render(it, listAdapter)
        })

        viewModel.getPopularMovies()
    }

    private fun render(viewState: MovieListViewModel.ViewState, listAdapter: MovieListAdapter) {
        listAdapter.submitList(viewState.movies?.data)
        swipeRefreshLayout.isRefreshing = (viewState.movies?.status == Resource.Status.LOADING)
        errorGroup.setVisible(viewState.movies?.status == Resource.Status.ERROR)
    }

    companion object {
        fun newInstance() = MovieListFragment()
    }

}

