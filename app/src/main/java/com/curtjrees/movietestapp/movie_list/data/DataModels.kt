package com.curtjrees.movietestapp.movie_list.data

import com.curtjrees.movietestapp.movie_list.domain.Movie
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MovieApi(
    @field:Json(name = "id") override val id: Int,
    @field:Json(name = "title") override val title: String
) : Movie

@JsonClass(generateAdapter = true)
data class MovieListApi(
    @field:Json(name = "page") val page: Int,
    @field:Json(name = "results") val results: List<MovieApi>
)