package com.curtjrees.movietestapp.movie_list.domain

interface Movie {
    val id: Int
    val title: String
}

interface MovieDataSource {

    suspend fun getMovie(id: Int): Movie

    suspend fun getPopularMovies(): List<Movie>

}