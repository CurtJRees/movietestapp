package com.curtjrees.movietestapp.movie_list

import com.curtjrees.movietestapp.movie_list.data.MovieApiService
import com.curtjrees.movietestapp.movie_list.domain.Movie
import com.curtjrees.movietestapp.movie_list.domain.MovieDataSource

class MovieRepository(
    private val service: MovieApiService
) : MovieDataSource {

    override suspend fun getMovie(id: Int): Movie = service.getMovie(id, API_KEY)

    override suspend fun getPopularMovies(): List<Movie> = service.getPopularMovies(API_KEY).results

    companion object {
        private const val API_KEY = "d1795749d541312943a339b042b11be8"
    }
}