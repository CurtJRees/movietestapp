package com.curtjrees.movietestapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.curtjrees.movietestapp.common.BaseFragment
import com.curtjrees.movietestapp.databinding.FragmentMovieDetailBinding

class MovieDetailFragment private constructor() : BaseFragment<FragmentMovieDetailBinding>() {

    override fun onInflateBinding(inflater: LayoutInflater): FragmentMovieDetailBinding = FragmentMovieDetailBinding.inflate(inflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

    }

    companion object {
        fun newInstance() = MovieDetailFragment()
    }


}